let today = document.getElementById("demo");
let todays = document.getElementById("start1");
let todays_stop = document.getElementById("stop1");
document.getElementById("start").innerHTML = "<div class='flex'><img src='../Img/play-button.png' class='w-6 h-auto mr-2'><span> Start</span></div>";


let Start_time = new Date();
let end_time=new Date();
let sum=0;


// display day
function time_day() {
    let day = new Date(); 
    let t_time = day.toDateString() + " " + day.toLocaleTimeString();
    day.innerHTML = t_time;
    document.getElementById("demo").innerHTML = t_time;
}

setInterval(time_day, 1000);
// start hour

function time_start() {
    Start_time  = new Date();
    let st =  Start_time.toLocaleTimeString("en-Us", {
    hour: "2-digit",
    minute: "2-digit",
  
  });
  document.getElementById("start1").innerHTML = "Stop";
  todays.innerHTML = st;


}
// stop hour
function time_stop() {
    end_time =new Date();
    let sp = end_time.toLocaleTimeString("en-Us", {
      hour: "2-digit",
      minute: "2-digit",
    });
    todays_stop.innerHTML = sp;
       document.getElementById("stop1").innerHTML = sp;

}
//function clear
function btn_clear() {
  document.getElementById("start").innerHTML = "<div class='flex'><img src='../Img/play-button.png' class='w-6 h-auto mr-2'><span> Start</span></div>";
  document.getElementById("start1").innerHTML = "Start ";
  document.getElementById("stop1").innerHTML= "Stop ";
  document.getElementById("min").innerHTML="0 Minutes";
  document.getElementById("money").innerHTML="0 Reils";
}
// declear variable
let min;//find mintues
let startBtn = true;
let stopBtn = false;
let clear = false;


function start() {
  if (startBtn) {
    document.getElementById("start").style.backgroundColor="red";
    document.getElementById("start").style.color="white";
    time_start();
    startBtn = false;
    stopBtn = true;
    document.getElementById("start").innerHTML = "<div class='flex'><img src='../Img/stop.png' class='w-6 h-auto mr-2'><span>Stop</span></div>";
  
  } else if (stopBtn) {
   
    document.getElementById("start").style.backgroundColor="yellow";
    document.getElementById("start").style.color="black";
    document.getElementById("start").innerHTML = "<div class='flex'><img src='../Img/delete.png' class='w-6 h-auto mr-2'><span>Clear</span></div>";
    time_stop();
    min=(end_time-Start_time)/60/1000;
     document.getElementById("min").innerHTML=parseInt(min);
    
     
   if(parseInt(min%60)>=0 && parseInt(min%60)<15)
   {
      sum=500;
     document.getElementById("money").innerHTML=sum;
   
   }
   else if(parseInt(min%60)>16 && parseInt(min%60)<30)
   {
      sum=1000;
     document.getElementById("money").innerHTML=sum;
   
   }
   else if(parseInt(min%60)>31 && parseInt(min%60)<60)
   {
      sum=1500;
     document.getElementById("money").innerHTML=sum;
   
   }
    let a=(parseInt(min/60))
    sum+=1500*a;

     document.getElementById("money").innerHTML=sum+" Reils";
   
   
    stopBtn = false;
    clear = true;

  } else if (clear) {
    document.getElementById("start").innerHTML = "<div class='flex'><img src='../Img/play-button.png' class='w-6 h-auto mr-2'><span> Start</span></div>";
   
    document.getElementById("start").style.backgroundColor="green";
    document.getElementById("start").style.color="white";
    startBtn = true;
    stopBtn = false;
    clear = false;
    btn_clear();
  }
}






